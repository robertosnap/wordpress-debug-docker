# FROM wordpress:latest

# ENV XDEBUG_PORT 9000

# RUN yes | pecl install xdebug && \
# echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini && \
# echo "xdebug.remote_enable=on" >> /usr/local/etc/php/conf.d/xdebug.ini && \
# echo "xdebug.idekey=vscode" >> /usr/local/etc/php/conf.d/xdebug.ini && \
# echo "xdebug.remote_autostart=1" >> /usr/local/etc/php/conf.d/xdebug.ini && \
# echo "xdebug.remote_connect_back =1" >> /usr/local/etc/php/conf.d/xdebug.ini && \
# echo "66.155.40.202 api.wordpress.org" >> /etc/hosts

# EXPOSE 9000

FROM wordpress:latest
LABEL maintainer André Costa <andreccosta@me.com>

ENV XDEBUG_PORT 9000
ENV XDEBUG_IDEKEY docker

RUN pecl install "xdebug" \
    && docker-php-ext-enable xdebug

RUN echo "xdebug.remote_enable=1" >> /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.remote_autostart=1" >> /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.remote_port=${XDEBUG_PORT}" >> /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.idekey=${XDEBUG_IDEKEY}" >> /usr/local/etc/php/conf.d/xdebug.ini

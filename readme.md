# Start
Make sure DOCKER is installed and docker (default) virtual machine is started.

First build our own debug wordpress enviroment with
```docker build -t wordpress-debug .```
Start wordpress and database run  ```docker-compose up -d```
Pause ```docker-compose down```

Destroy ```docker-compose down --volumes ```

# Links
Taken from here https://docs.docker.com/compose/wordpress